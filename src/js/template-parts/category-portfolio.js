
import $ from "jquery"
import makeItemsFiltered from "../categorizer/makeItemsFiltered";
import makeMasonry from "../responsive/makeMasonry";
import getHash from "../utils/getHash";
import setHash from "../utils/setHash";

export default function applyPortfolioScript(detector){
    if(!$("body.category-portfolio").length) return;

    // const pormsn=makeMasonry(".items-portfolio-container",".item-container",detector);
    const portfolioFilters = makeItemsFiltered(".items-portfolio-container .item-container",detector,{
        // showFunction:pormsn.addItem,
        // hideFunction:pormsn.removeItem,
    });

    const tagsFilters = makeItemsFiltered(".items-tags-container .item-container",detector);   

    let currentSuperTagFilter=false;

    const activatedSuperTags=new Set();

    detector.onDomReady(() => {
        //make tag items filter portfolio items
        const activatedTags=new Set();

         /** 
         * show only those tags that are present in at least one post among the ones being shown
         * it is a "reverse" filter because items are filtered according not to their tags, but the tags they select.
         * a "reverseTag" property needs to have been injected
         * @param {Set<String>} tags
         * */
        const reverseFilter=(tags)=>{
            tagsFilters.forEach((element)=>{
                element.displayIfTrue(tags.has(element.reverseTag));
            });
        }
        const tagButton$={};

        const tagSelection=(tag)=>{
            $(".item-tag-container.active").each(deactivateTag);
            const $button=tagButton$[tag];
            if(!$button) return;
            activateTag($button);
            portfolioFilters.showContainingAny([tag]);
        }

        //helpers to apply the appearance and register active state of filter
        function deactivateTag($el){
            $el.removeClass("active");
            activatedTags.delete($el);
        }

        function activateTag($el){
            activatedTags.add($el);
            $el.addClass("active");
        }


        //append an event to each tag element so that when clicked, it performs a filter.
        tagsFilters.forEach(function(element){

            let $this=element.$el;
            let myTag=$this.attr("data-slug");
            //inject a "reverseTag" property for reverse filter
            element.reverseTag=myTag;
            
            //if this tag would result in 0 posts, remove it
            if(!portfolioFilters.tagsList.has(myTag)){
                $this.detach();
                return;
            }

            tagButton$[myTag]=$this;

            $this.on("click",function(evt){
                evt.preventDefault();
                
                let active=activatedTags.has($this);
                //only one active tag at a time allowed, represent that:
                activatedTags.forEach(deactivateTag);

                if(active){
                    deactivateTag($this);
                    portfolioFilters.showAll();
                    if(currentSuperTagFilter){
                        //keep respecting supertag selection
                        tagsFilters.showContainingAny([currentSuperTagFilter]);
                    }else{
                        tagsFilters.showAll();
                    }
                    setHash(false);
                }else{

                    setHash(`${myTag}`);
                    // tagSelection(myTag);
                    // reverseFilter(portfolioFilters.getItemTagsCurrentlyShowing());
                }
            });
        });



        //make supertag elements

        const superTagButton$={};
        //helpers to apply the appearance and register active state of filter
        function activateSuperTag($el){
            $el.addClass("active");
            activatedSuperTags.add($el);
        }
        function deactivateSuperTag($el){
            $el.removeClass("active");
            activatedSuperTags.delete($el);
        }

        const $superTagsEl=$(`<div class="items-container items-supertags-container"></div>`);
        $(".items-tags-container .item-container").parent().prepend($superTagsEl);
        const superTagSelection=(supertag)=>{
            activatedSuperTags.forEach(deactivateSuperTag);
            const $superTagButton=superTagButton$[supertag];
            currentSuperTagFilter=supertag;
            if(!$superTagButton) return;
            tagsFilters.showContainingAny([supertag]);
            activateSuperTag($superTagButton);
        }
        tagsFilters.tagsList.forEach(function(supertag){
            if(supertag=="") return;
            const tagName = supertag==""?"other":supertag;
            const $this=$(`<a href="#${tagName}_" class="item-container item-supertag-container tag ${tagName}">${tagName}</a>`);
            $this.appendTo($superTagsEl);

            superTagButton$[tagName]=$this;

            $this.on("click",(evt)=>{

                const active=activatedSuperTags.has($this);
                
                if(active){
                    tagsFilters.showAll();
                    // deactivateSuperTag($this);
                    // currentSuperTagFilter=false;
                    superTagSelection(false);

                }else{
                    superTagSelection(supertag);
                    // setHash(`${tagName}_`);
                }
                
                evt.preventDefault();
            });
            
            //special effect where hovering a supertag highlights the would-be-selected tags
            $this.on("mouseenter",()=>{
                $(`.tag.${tagName}`).addClass("hover");
            });
            $this.on("mouseleave",()=>{
                $(`.tag.${tagName}`).removeClass("hover");
            });

        });

        //url hash related functionality
        const rehash=()=>{
            const hash = getHash();
            let urlSupertag = false;
            let urlTag = false;
            if(hash){
                const split=hash.split("_");
                urlTag = hash;
                split.pop();
                urlSupertag = split.pop();
                if(urlSupertag==undefined) urlSupertag=false;
                console.log({hash,urlSupertag,urlTag});
            }
            
            if(urlTag){
                tagSelection(urlTag);
                setTimeout(()=>{
                    $(".items-container.items-tags-container").click();
                },100);
            }
            if(urlSupertag){
                //
                // superTagSelection(urlSupertag);
                // setTimeout(()=>{
                //     $(".items-container.items-tags-container").click();
                // },100);
            }
        }

        $(window).on('hashchange',rehash);
        rehash();

    });
};