import $ from "jquery";
require('jquery-ui-bundle');

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"

/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const makeDraggable=function(selector,eventsDetector){
    $(selector).each(function(){
        $(this).draggable();
    })
};

export default makeDraggable;