import $ from "jquery";
require('jquery-ui-bundle');

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"

/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const makeDraggableSpringyX=function(selector,eventsDetector){
    $(selector).each(function(){
        $(this).draggable({ revert: true,axis:"x"});
    });
}
export default makeDraggableSpringyX;
