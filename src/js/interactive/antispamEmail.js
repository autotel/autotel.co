import $ from "jquery";
require('jquery-ui-bundle');

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"
/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const antispamEmail=function(selector,eventsDetector){
    const obfuscate=(str)=>btoa(str);
    const deobfuscate=(str)=>atob(str);
    const val="am9hcXVpbi5hbGR1bmF0ZUBnbWFpbC5jb20=";
    
    $(selector).each(function(){
        const $th=$(this);
        const $el=$(`<div class="contact-switcher">Display e-mail</div>`);
        const $switchEl=$(`<div class="knob">→</div>`);
        const $contents=$(`<div class="content"></div>`);
        const sizebase=1.7;
        const sizeUnit="rem";
        $el.css({
            width:15*sizebase+sizeUnit,
            // "background-color":"gray",
            height:1.1*sizebase+sizeUnit,
            position:"relative",
            // color:"white",
            color:"gray",
            border:"solid 1px",
            "font-size": sizebase+sizeUnit,
            "line-height": sizebase+sizeUnit,
            "border-radius":sizebase+sizeUnit,
            "text-align":"right",
            "padding-right":0.25*sizebase+sizeUnit,
            overflow:"hidden",
            "display": "inline-block",
        });

        $switchEl.css({
            width:2.3*sizebase+sizeUnit,
            "position":"absolute",
            top:"0px",
            "background-color":"#1984d2",
            height:"100%",
            // "border-radius":"15px",
            color:"white",
            "text-align":"center",
            "cursor":"pointer",
            // "font-family":"Roboto Sans-serif",

        });

        $switchEl.appendTo($el);
        $contents.appendTo($el);
        $el.appendTo($th);


        const conclude=()=>{
            $contents.text(deobfuscate(val));
            $switchEl.fadeOut();
            $el.html("");

            $contents.appendTo($el);
            $contents.text(deobfuscate(val));
        }

        const dragEndAction=()=>{
            $switchEl.css("left",0);
            dragAction();
        }
        const dragAction=()=>{
            const left=parseInt($switchEl.css("left"));
            const dragmax=250;

            $switchEl.css({
                opacity:Math.min(1,1.3-(left/dragmax)),
            });

            if(left>dragmax){
                conclude();
            }
        }
        $switchEl.draggable({
            axis: "x",
            drag:(e)=>dragAction(),
            stop:(e)=>dragEndAction(),
        });
    });

};

export default antispamEmail;