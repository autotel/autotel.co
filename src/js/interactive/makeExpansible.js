import $ from "jquery";
require('jquery-ui-bundle');

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"

/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 * @param {{closedHeight:number?}} eventsDetector
 */

const makeExpansible=function(selector,eventsDetector,customSettings={}){
    const settings={
        closedHeight:64,
        buttonOffset:0,
    }
    Object.assign(settings,customSettings);

    function Expansible($el,height=settings.closedHeight){

        let originalHeight=$el.height();
        let expanded=false;
        $el.css({height,overflow:"hidden"});
        const $button=$('<div class="button button-expand">▼</button>');
        $button.css({top:(settings.buttonOffset)+"px"});
        $el.prepend($button);
        
        function recalcHeight(){
            var rh=$el.height();
            $el.css({height:""});
            originalHeight=$el.height();
            $el.css({height:rh});
        }
        function expand(){
            if(expanded) return;
            recalcHeight();
            $el.addClass("expanded");
            $button.text("▲");
            expanded=true;
            $el.animate(
                {height:originalHeight},
                {complete:function(){
                    $el.css({
                        height:"",
                    });
                    eventsDetector.reportLayoutChange();
                }});
        }
        function contract(){
            if(!expanded) return;
            $el.removeClass("expanded");
            $button.text("▼");
            expanded=false;
            $el.animate({height});
            setTimeout(()=>eventsDetector.reportLayoutChange());
        }
        $el.on("click",function(event){
            event.preventDefault();
            event.stopPropagation();

            expand();
        });
        $button.on("click",function(event){
            event.stopPropagation();
            if(expanded){
                contract();
            }else{
                expand();
            }
        });
    }
    
    $(selector).each(function(){
        new Expansible($(this));
    });
};

export default makeExpansible;