import * as $ from "jquery"
/*
This script takes data from a set of html items, and classifies it, allowing the user to navigate using this information as filtering criteria.
this is used in the pilots page, and is what makes the tag-based filtering possible.
*/

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"
import getHash from "../utils/getHash";

/** a dom element that might be displayed or not according to filter settings */
const FilteredElement=function($el){

    this.tags=$el.attr("data-tags").split(/ +/g);
    this.$el=$el;
    const $parent=$el.parent();
    // const $debugEl=$(`<div class="debug" style="background-color:rgba(255,255,255,0.5); position:absolute; top:0px">${this.tags.join("  ")}</div>`);
    // $debugEl.appendTo($el);
    let shown=undefined;
    this.isShown=()=>shown;

    this._show=function(){
        if(shown===true) return;
        shown=true;
        this.show($el);
    }

    this._hide=function(){
        if(shown===false) return;
        shown=false;
        this.hide($el);
    }

    this.show=function(){
        $parent.append($el);
    }
    this.hide=function(){
        $el.detach();
    }

    const intersectArrays=(a,b)=>a.filter(x => b.includes(x));

    this.containsAny=(tagsList)=>{
        if(intersectArrays(tagsList,this.tags).length>0){
            return true;
        }else{
            return false;
        }
    }

    this.containsEvery=(tagsList)=>{
        if(intersectArrays(tagsList,this.tags).length===tagsList.length){
            return true;
        }else{
            return false;
        }
    }

    this.displayIfTrue=(val)=>{
        // console.log("displayiftrue",$val);
        if(val){
            this._show();
        }else{
            this._hide();
        }
    }

    this.showIfContainsAny =
        (tagsList)=>this.displayIfTrue(this.containsAny(tagsList));
    this.showIfContainsEvery =
        (tagsList)=>this.displayIfTrue(this.containsEvery(tagsList));
}


/**
 * @param {string} selector, selector for elements which must contain a "data-tags" property.
 * @param {PageEventsDetector} eventsDetector
 * @param {{hideFunction:Function,showFunction:Function}} otherProperties
 */
export default function makeItemsFiltered(selector, eventsDetector, otherProperties={}) {
    const everyElement=[];
    const tagsList=new Set();
    /**
     * @param {Function(el:FilteredElement)} cb
     */
    const forEach=(cb)=>everyElement.map((el)=>cb(el));
    /** 
     * get a union of the list of all the tags present in all those elements that are currently shown
     * this targets specifically the funcion of filtering the tags to show only the relevant ones to the current tag
     */
    function getItemTagsCurrentlyShowing(){
        let shownItems=everyElement.filter((el)=>el.isShown());
        let shownTags=new Set((shownItems.map((item)=>item.tags)).flat());
        return shownTags;
    }
    function getContainingEvery(tagsList){
        return everyElement.filter((el)=>el.containsEvery(tagsList));
    }
    function getContainingAny(tagsList){
        return everyElement.filter((el)=>el.containsEvery(tagsList));
    }
    function showContainingEvery(tagsList){
        everyElement.map((el)=>el.showIfContainsEvery(tagsList));
        eventsDetector.reportLayoutChange();
    }
    function showContainingAny(tagsList){
        everyElement.map((el)=>el.showIfContainsAny(tagsList));
        eventsDetector.reportLayoutChange();
    }
    function showAll(){
        everyElement.map((el)=>el._show());
        eventsDetector.reportLayoutChange();
    }
    
    window.selectByTags=function(tags){
        everyElement.map((el)=>el.showIfContainsAny(tags));
    }
    
    eventsDetector.onDomReady(() => {
        console.log("categorizer.js");
        $(selector).each(function(){
            const nel=new FilteredElement($(this));
            everyElement.push(nel);
            nel.tags.map((tt)=>tagsList.add(tt));
            if(otherProperties.hideFunction) nel.hide = otherProperties.hideFunction;
            if(otherProperties.showFunction) nel.show = otherProperties.showFunction;

        });

    });

    return {
        showContainingAny,
        showContainingEvery,
        getContainingEvery,
        getContainingAny,
        showAll,
        tagsList,
        getItemTagsCurrentlyShowing,
        forEach,
    }   
};