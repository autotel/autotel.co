import $ from "jquery"
import jQueryBridget from 'jquery-bridget';
import Masonry from "masonry-layout"

// make Masonry a jQuery plugin
jQueryBridget( 'masonry', Masonry, $ );

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"


/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const makeMasonry = function (parentSelector, childrenSelector, eventsDetector) {
    // const msnry = new Masonry(parentSelector, {
    //     // set itemSelector so .grid-sizer is not used in layout
    //     itemSelector: childrenSelector,
    //     // use element for option
    //     // columnWidth: childrenSelector,
    //     // percentPosition: true
    // });

    const $grid=$(parentSelector)
    $grid.masonry();
    
    // window.msnry=msnry;
    
    const addItem=($it)=>{
        $grid.append($it).masonry("appended",$it);
        $grid.masonry("layout");
        // console.log("addItem",$it);
        // msnry.addItems($it);
        // msnry.appended($it);
        // relayout();
    }
    const removeItem=($it)=>{
        $it.detach();
        // msnry.remove($it);
        relayout();
    }
    eventsDetector.onDomReady(()=> relayout() );

    // const appendNewElements = () => {
    //     $(childrenSelector).each(() => {
    //         (function ($) {
    //             let $this = $(this);
    //             if (!$this.hasClass("masonry-appended")) {
    //                 $this.addClass("masonry-appended");
    //                 msnry.addItems($this);
    //             }
    //         })
    //     });
    // }

    // appendNewElements();

    const relayout = () => {
        // msnry.layout();
    }

    eventsDetector.onElementAdded(() => {
        // appendNewElements();
    });
    eventsDetector.onWindowResize(() => {
        // relayout();
    });

    return {
        addItem,
        removeItem,
    }
}

export default makeMasonry;