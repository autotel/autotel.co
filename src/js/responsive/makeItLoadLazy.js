import $ from "jquery"

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"


/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const makeItLoadLazily=function(selector,eventsDetector,customSettings={}){
    const settings={
        amount:5,
        fadein:100,
        initialAmount:5,
    }
    Object.assign(settings,customSettings);
    // const $el=$(selector);
    const $children=$(selector).children();
    // console.log($children);

    function Lazify($el){
        Lazify.list.push(this);
        let original=$el.html();
        $el.html("");
        // console.log("lazify",$el);
        $el.hide();
        this.appear=function(){
            // console.log("Appear");
            $el.html(original);
            $el.fadeIn(settings.fadein);
            eventsDetector.reportElementsCreated($el);
        }
    }
    /** @type {Lazify[]}  */
    Lazify.list=[];
    
    let lastShown=0;
    let nearEnd=false;
    eventsDetector.onScroll(()=>{
        if(nearEnd) makeElementsAppear();
    });
    eventsDetector.onScrollEnterNearEnd(()=>{
        nearEnd=true;
        // console.log("lazy load start");
        makeElementsAppear();
    });
    eventsDetector.onScrollLeaveNearEnd(()=>nearEnd=false);
        
    $($children).each(function(){
        new Lazify($(this));
    });

    function makeElementsAppear(count=settings.amount){
        for(let a=0; a<count; a++){
            if(Lazify.list[lastShown]){
                Lazify.list[lastShown].appear();
                // console.log(lastShown);
                lastShown++;
            }
        };
        setTimeout(()=>{
            eventsDetector.reportLayoutChange();
        },settings.fadein);
    }

    makeElementsAppear(settings.initialAmount);

    // let lastAppeared=0;
    // let appearEach=5;

    // const onEndReached=()=>{
    //     for(let a=lastAppeared; a<lastAppeared+appearEach; a++){
    //         postsFilterGroup.list[lastAppeared+a].appear();
    //         postsFilterGroup.list[lastAppeared+a].$el.fadeIn();
    //     }
    //     lastAppeared+=appearEach;
    //     //depends on responsive.js
    //     if(triggerResponsive) triggerResponsive();
    // }

}
export default makeItLoadLazily;