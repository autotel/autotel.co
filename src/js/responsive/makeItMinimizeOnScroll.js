import $ from "jquery";

//imported only for types:
import PageEventsDetector from "../PageEventsDetector"

/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */

const makeItMinimizeOnScroll=function(selector,eventsDetector){
    let lastState=false;
    eventsDetector.onScroll(({top,left})=>{
        let currentState=top>5;
        if(currentState!=lastState){
            lastState=currentState;
            if(currentState){
                $(selector).addClass("minimized");
                setTimeout(()=>eventsDetector.reportLayoutChange(),300);
            }
        }
    });
}

export default makeItMinimizeOnScroll;