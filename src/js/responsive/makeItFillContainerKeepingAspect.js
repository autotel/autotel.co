import $ from "jquery";
//only for type:
import PageEventsDetector from "../PageEventsDetector";
/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const makeItFillContainerKeepingAspect=function(selector,eventsDetector){
    function ForceAspectFullWidth($th){
        if(!$th[0])return;
        if($th.attr("data-appended-force-aspect")=="true") return;
        $th.attr("data-appended-force-aspect","true");
        console.log("will adjust the size of:", $th);

        var atW=parseInt($th.attr("width"));
        var atH=parseInt($th.attr("height"));

        var aspect=$th.width() / $th.height();

        if(atW && atH){
            aspect=atW/atH;
        }
        
        var originalWidth = $th.width();
        var adoptedWidth=false;

        const respond=function(){
            var parentWidth=$th.parent().width();
            if (adoptedWidth!==parentWidth) {
                adoptedWidth=parentWidth;
                // $th.attr("width",parentWidth);
                // $th.attr("height",parentWidth/aspect);
                $th.css({
                    width:parentWidth,
                    height:parentWidth/aspect
                });
            }
        }
        eventsDetector.onWindowResize(()=>{
            respond();
        });
    }


    $(selector).each(function(){
        new ForceAspectFullWidth($(this));
    });
    
}
export default makeItFillContainerKeepingAspect;