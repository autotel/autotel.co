import $ from "jquery";

//only for type:
import PageEventsDetector from "../PageEventsDetector";

/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const makeHeightEqualToWidth=function(selector,eventsDetector){
    function recalc(over){
        $(over).find(selector).addBack(selector).each(function(el){
            const $el=$(this);
            // console.log("makeHeightEqualToWidth",$el[0]);
            $el.css({
                height:$el.width()+"px"
            });
        });
    }
    eventsDetector.onElementAdded((element)=>{
        recalc(element);
    });
    eventsDetector.onLayoutChange(()=>recalc($("body")));
    eventsDetector.onWindowResize(()=>recalc($("body")));
    
}
export default makeHeightEqualToWidth