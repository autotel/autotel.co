import $ from "jquery";

// $.fn.find2 = function(selector) {
//     return this.filter(selector).add(this.find(selector));
// };

//only for type:
import PageEventsDetector from "../PageEventsDetector";
import throttle from "../utils/throttle";

/**
 * @param {string} selector
 * @param {PageEventsDetector} eventsDetector
 */
const makeItFillContainer=function(selector,eventsDetector){
    function FillElement($th){
        if(!$th[0])return;//||!$th.selector
        // $th=$($th[0]);
        if($th.hasClass("full-width")||$th.hasClass("full-height")) return;
        // var $th = $(this);
        // console.log("makeItFillContainer of:", $th);
        let aspect=$th.width() / $th.height();
        let cachedState;
        let $parent=$th.parent();
        function setFh(){
            $th.removeClass("full-width");
            $th.addClass("full-height");
        }
        function setFw(){
            $th.addClass("full-width");
            $th.removeClass("full-height");
        }
        const respond = throttle(function(){
            if (aspect) {
                let parentAspect =  $parent.width()/$parent.height();
                // console.log($parent)
                // console.log(parentAspect, $parent.width(),$parent.height());
                if (parentAspect < aspect !== cachedState){
                    if (parentAspect < aspect){
                        setFh();
                    }else{
                        setFw();
                    }
                }
            };
        },100);

        eventsDetector.onWindowResize(()=>{
            respond();
        });
        eventsDetector.onImagesLoaded(()=>{
            respond();
        });
        eventsDetector.onLayoutChange(()=>{
            respond();
        });
    }

    //find elements that are supposed to fill container
    function findAndActivate(){
        $(selector).each(function(){
            new FillElement($(this));
        });
    }
    eventsDetector.onImagesLoaded(()=>{
        findAndActivate();

        eventsDetector.onElementAdded((element)=>{
            findAndActivate();
        });
    });

}

export default makeItFillContainer;