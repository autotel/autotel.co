import PageEventsDetector from "../PageEventsDetector";
import throttle from "../utils/throttle";
import $ from "jquery";
const fitText = require("FitText-UMD");

/** only for scarce title text!! */
/** 
 * @param {string} selector
 * @param {PageEventsDetector} detector 
 **/
const makeTextFit=(selector,detector)=>{
    const refit=throttle(()=>{
        $(selector).each(function(){
            fitText( this ,0.9);
        });
    },100);
    detector.onWindowResize(refit);
    detector.onLayoutChange(refit);
    detector.onDomReady(refit);
    refit();
}
export default makeTextFit;