/** @returns {string|false} */
const getHash=()=>{
    if (!window.location.hash) return false;
    return decodeURIComponent(window.location.hash)
        .replace(/^\#/, "")
        .toLowerCase();
}
export default getHash;