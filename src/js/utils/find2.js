import $ from "jquery";

$.fn.find2 = function(selector) {
    return this.filter(selector).add(this.find(selector));
};