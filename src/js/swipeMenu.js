import $ from "jquery";

(function(){
    let linksRoulette=[
        "home",
        "porfolio",
        "blog",
        "music",
    ];
    let currentLink=2;
    $el=$('<div id="swipe-menu">sw</div>');

    $el.css({
        position:"absolute",
        "width":"100%",
        "height":"200px",
        "z-index":11,
        // "background-color":"red",
        "text-align":"center",
    });
    function wraparound(val,max){
        if(val>max) val%=max;
        if(val<0) val=max-1;
        return val;
    }
    function switchAction(delta){
        currentLink=wraparound(currentLink+delta,linksRoulette.length);

        $el.html(
            linksRoulette[wraparound(currentLink-1,linksRoulette.length)]
            +"<h1>"
            +linksRoulette[currentLink]
            +"</h1>"
            +linksRoulette[wraparound(currentLink+1,linksRoulette.length)]);
    }
    let lengaged=false;
    let rengaged=false;
    $("body").on("drag",function(ev){
        var left=parseInt($("body").css("left"));
        //apparently there isnt other way to get direction of the swipe
      
        if(left<-300){
            if(!lengaged) switchAction(-1);
            lengaged=true;
        }else{
            lengaged=false;
        }
        if(left>300){
            if(!rengaged) switchAction(1);
            rengaged=true;
        }else{
            rengaged=false;
        }
    });
    $("body").append($el);
    $("body").draggable({ revert: true,axis:"x",handle:$el});
})();