import $ from "jquery";
(function($){

    $(document).ready(function(){
        const $menuEl=$("#primary-menu");
        $menuEl.detach().appendTo($("body"));
        $menuEl.css({position:"absolute",top:"0px"});

        var lastScroll=0;
        var menuHeight=parseInt($menuEl.outerHeight());
        var stuck=false;
        $(document).on("scroll",function(evt){
            if(!menuHeight) menuHeight=parseInt($menuEl.outerHeight());
            // console.log(evt.originalEvent);
            var pageY=evt.originalEvent.pageY;
            var delta=pageY-lastScroll;
            var menuTop=$menuEl.offset().top;
            var menuBottom=menuTop+menuHeight;
            // console.log({
            //     pageY,
            //     delta,
            //     menuTop,
            //     menuBottom,
            // });
            if(delta<0){
                //up
                if(pageY<=menuTop){
                    if(!stuck){
                        stuck=true;
                        $menuEl.css({position:"fixed",top:1+"px"});
                        // console.log("R1");
                    }
                }else if(stuck){
                    stuck=false;
                    $menuEl.css({position:"absolute",top:(pageY-menuHeight-1)+"px"});
                    // console.log("R2");
                }
            }else{
                //down
                if(pageY>menuBottom){
                    //reached bottom, fix
                    stuck=true;
                    // $menuEl.css({position:"fixed",top:});
                }else if(stuck){
                    //not reached, absolute (once)
                    stuck=false;
                    $menuEl.css({position:"absolute",top:(pageY)+"px"});
                    // console.log("R3");
                }
            }

            lastScroll=pageY;
        });
    });

})($);
export default "";