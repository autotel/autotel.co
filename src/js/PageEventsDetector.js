import $ from "jquery"
import throttle from "./utils/throttle";
import imagesLoaded from'imagesloaded';

const Waiter=function(){
    let fulfilled=false;
    
    let listeners=[];
    
    const triggerAll=(...p)=>{
        listeners.map((cb)=>cb(...p));
    }

    this.completed=(...p)=>{
        fulfilled=p;
        triggerAll(...p);
    }

    this.incomplete=(...p)=>{
        fulfilled=false;
    }
    
    this.isCompleted=()=>fulfilled?true:false;

    this.addTask=(cb)=>{
        listeners.push(cb);
        if(fulfilled){
            cb(...fulfilled);
        }
    }
    /** untested, removes a previously added task callback cb */
    this.removeTask=(cb)=>{
        const foundIndex=listeners.indexOf(cb);
        if(foundIndex!==-1){
            listeners.splice(foundIndex,1);
        }else{
            console.warn("failed to remove listener",cb);
        }
    }
    this.makeOnFunction=()=>{
        const self=this;
        return function(callback){
            self.addTask(callback);
            return {
                off:()=>{
                    self.removeTask(callback);
                }
            }
        }
    }
    
}

const PageEventsDetector=function(){
    //setup document waiters
    const imagesLoadedWaiter=new Waiter();
    const scrollWaiter=new Waiter();
    const domReadyWaiter=new Waiter();
    const windowResizeWaiter=new Waiter();
    const scrollNearEndReached=new Waiter();
    const scrollNearEndLeft=new Waiter();
    const newElementsCreated=new Waiter();
    const layoutChangeWaiter=new Waiter();
    //store immutable $ elements
    const $document=$(document);
    const $window=$(window);
    //vars where to cache responsive relevant document data
    let documentHeight=0;
    let documentWidth=0;
    let windowHeight=0;
    let windowWidth=0;

    //functions to update responsive data, and call them right away
    const documentSizeChangedHandler=throttle(function(){
        documentWidth=$document.width();
        documentHeight=$document.height();
        windowWidth=$window.width();
        windowHeight=$window.height();
        windowResizeWaiter.completed({
            width:windowWidth,
            height:windowHeight,
        });

    },100);

    /**
     * @param {$[]} elements list
     * */
    const newElementsCreatedHandler = function(elements){
        // elements.map((element)=>{
            // console.log("page events detector: new element");
            newElementsCreated.completed(elements);
        // });
    }
    const scroll={
        top:$document.scrollTop(),
        left:$document.scrollLeft(),
    }
    const checkWhenterScrollEnd=()=>{
        const isNearEnd=scroll.top + windowHeight > documentHeight - 300;
        const wasNearEnd=scrollNearEndReached.isCompleted();

        if(isNearEnd!=wasNearEnd){
            if(isNearEnd){
                scrollNearEndReached.completed(scroll);
                scrollNearEndLeft.incomplete(scroll);
            }else{
                scrollNearEndLeft.completed(scroll);
                scrollNearEndReached.incomplete(scroll);
            }
        }
    }
    const documentScrolledHandler=throttle(function(){

        scrollWaiter.completed(scroll);
        checkWhenterScrollEnd();
        // console.log(`check near end ${scroll.top} + ${windowHeight} > ${documentHeight} - 300`);

    },100);

    $document.on("scroll",function(evt){
        scroll.top=$document.scrollTop();
        scroll.left=$document.scrollLeft();
        documentScrolledHandler();
    });

    $document.ready(function () {
        domReadyWaiter.completed();
        checkWhenterScrollEnd();
    });

    $window.on("resize",function(evt){
        documentSizeChangedHandler();
    });

    imagesLoaded( document, function() {
        imagesLoadedWaiter.completed();
        documentSizeChangedHandler();
        documentScrolledHandler();
    });
    
    this.onImagesLoaded=imagesLoadedWaiter.makeOnFunction();
    this.onScroll=scrollWaiter.makeOnFunction();
    this.onDomReady=domReadyWaiter.makeOnFunction();
    this.onDomReady(()=>console.log("domready"));

    this.onWindowResize=windowResizeWaiter.makeOnFunction();
    this.onScrollEnterNearEnd=scrollNearEndReached.makeOnFunction();
    this.onScrollLeaveNearEnd=scrollNearEndLeft.makeOnFunction();
    this.onElementAdded=newElementsCreated.makeOnFunction();
    this.onLayoutChange=layoutChangeWaiter.makeOnFunction();
    /** call this after changing the layout so all the affected elements can update*/
    this.reportLayoutChange=function(){
        documentSizeChangedHandler();
        layoutChangeWaiter.completed();
    };
    /** call this after programmatically scrolling the document to update all the affected elements */
    this.reportScroll=function(){
        documentScrolledHandler();
    };
    /**
     * call this after programmatically scrolling the document to update all the affected elements
     * @param {$[]|$} elements the created elements. If skipped, the new elements might miss some characteristics
     * */
    this.reportElementsCreated=function(elements){
        if(! Array.isArray(elements)) elements=[elements];
        //find wrongly fed elements
        // if (process.env.NODE_ENV !== 'production') {
        //     elements.map((element)=>{
        //         if(!element instanceof jQuery){
        //             console.log("with:",element);
        //             throw new Error("claimed new element not a $ object")
        //         }
        //     });
        // }

        newElementsCreatedHandler(elements);
    };

};
export default PageEventsDetector;