import PageEventsDetector from "./PageEventsDetector";
import makeDraggableSpringyX from "./interactive/makeDraggableSpringyX";
import makeExpansible from "./interactive/makeExpansible";
import makeItFillContainer from "./responsive/makeItFillContainer";
import makeItMinimizeOnScroll from "./responsive/makeItMinimizeOnScroll";
import makeItFillContainerKeepingAspect from "./responsive/makeItFillContainerKeepingAspect";
import makeItLoadLazily from "./responsive/makeItLoadLazy";
import makeHeightEqualToWidth from "./responsive/makeHeightEqualToWidth";
// still have to make these parametric:
import mainMenu from "./mainMenu";
import slf from "./skip-link-focus-fix";
import makeItemsFiltered from "./categorizer/makeItemsFiltered";
import makeMasonry from "./responsive/makeMasonry";
import applyPortfolioScript from "./template-parts/category-portfolio";
import antispamEmail from "./interactive/antispamEmail";
import makeTextFit from "./responsive/makeTextFit";

const detector=new PageEventsDetector();


makeItLoadLazily('.items-blogposts-container',detector);
makeItLoadLazily('.items-music-container',detector);
// makeItLoadLazily('.items-portfolio-container',detector,{
//     amount:8,
//     initialAmount:12,
// });

// for portfolio page
// makeDraggableSpringyX("body.category-portfolio .preview",detector);
makeExpansible("body.category-portfolio .items-tags-container",detector,{
    closedHeight:44,
    buttonOffset:-44,
});
makeExpansible(".expansible",detector,{
    closedHeight:44
});

makeHeightEqualToWidth('.items-portfolio-container .item-container',detector);
makeHeightEqualToWidth('.items-container.shortcode.grid3d .item-container',detector);
// makeItFillContainer('.items-portfolio-container .preview-el',detector);

makeTextFit('.items-container.shortcode.grid3d p.post_title',detector);
makeTextFit('.items-portfolio-container h2.title',detector);

// makeItFillContainer('body.page-template-default .item-container img',detector);
makeItFillContainer('body.page-template-default .items-music-container img',detector);
makeItFillContainer('.items-container .preview img',detector);
// makeItFillContainer('.items-webdesign-container .thumbnail',detector);
makeItFillContainer('.items-container.shortcode.grid3d .thumbnail',detector);

// makeItMinimizeOnScroll(".section-title-container",detector);

makeItFillContainerKeepingAspect('iframe.full-width',detector);

applyPortfolioScript(detector);

antispamEmail(".anti-spam-email",detector);
/**
 * since I am really programming how things look and are laid out, I could use a 
 * css like syntax with the added benefit of being able to append properties thus 
 * removing some functions such as "makeDraggableSpringyX" (because it could be
 * draggable, with properties "springy" and constrain x)
 * 
 * Something like, I don't know:
 * 
 * {
 *      .items-portfolio-container .preview-el {
 *            fillsContainer: keepsAspect, force
 *      }
 * }
 * 
 */