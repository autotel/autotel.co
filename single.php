<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autotel2019
 */

get_header();

wp_localize_script( 'miniUi', 'selectors', array(
	'expansible' => ".dissappearing, .expansible" //needs re-naming. Held by "portfolio" site
	//needs addition of custom-gallery
)); 


?>


	<?php
	while ( have_posts() ) :
		the_post();

		?>


		<section class="section-container section-title-container">
			<?php the_title( '<h1 class="title">', '</h1>' ); ?>
		</section>
		<section class="section-container section-metadata-container">
			<div class="items-container items-tags-container">
			<?php
				$terms = get_the_tags();
				foreach ($terms as $key => $term) {
					/*
					example:
					[term_id] => 53 
					[name] => 2010 
					[slug] => year_2010 
					[term_group] => 0 
					[term_taxonomy_id] => 53 
					[taxonomy] => post_tag 
					[description] => 
					[parent] => 0 
					[count] => 3 
					[filter] => raw 
					*/
					?>
					<a class="item-container item-tag-container tag <?php
						//add a class according to before the underscore
						$strpos=strpos($term->slug,"_");
						if($strpos){
							echo substr($term->slug,0,$strpos);
						}?>"<?php
						echo  ' data-slug="'.$term->slug.'"';
						echo  ' data-taxonomy="'.$term->taxonomy.'"';
						echo  ' data-count="'.$term->count.'"';
						echo  ' data-term_id="'.$term->term_id.'"';
					?> href="<?php
						echo  ' https://autotel.co/tag/'.$term->slug;
					?>"><?php echo $term->name; ?></a>
					<?php
				}
			?>
			</div>
		</section>

		<section class="section-container section-content-container">
		

			<?php
			the_content();
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'autotel2019' ),
				'after'  => '</div>',
			) );
			?>
		</section>
		<section class="section-container section-metadata-container">
			<?php if ( get_edit_post_link() ) : ?>
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'autotel2019' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
			<?php endif; ?>
		</section>
		<section class="section-container section-comments-container">
			<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			endwhile; // End of the loop.
			?>
		</section>

<?php
get_footer();
