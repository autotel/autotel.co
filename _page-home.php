<?php

get_header();

?>
<!--page-home.php-->
	<?php
	while ( have_posts() ) :
		include(get_template_directory_uri() . "/inc/hometoy.html");
		the_post();

		?>
		<section class="section-container section-content-container">

			<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php autotel2019_post_thumbnail(); ?>

			<?php
			the_content();
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'autotel2019' ),
				'after'  => '</div>',
			) );
			?>
		</section>
		<section class="section-container section-metadata-container">
			<?php if ( get_edit_post_link() ) : ?>
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'autotel2019' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
			<?php endif; ?>
		</section>
		<section class="section-container section-comments-container">
			<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			endwhile; // End of the loop.
			?>
		</section>

<?php
get_footer();
