<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package autotel2019
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="initial-scale=0.7">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>  xml:lang="en-US">
<div class="main-container">

	<header class="section-container section-header-container">
		<?php
		the_custom_logo();
		?>
		<h1 class="site-title">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
			
		</h1>
		<?php
		$autotel2019_description = get_bloginfo( 'description', 'display' );
		if ( $autotel2019_description || is_customize_preview() ) :
			?>
			<p class="site-description"><?php echo $autotel2019_description; /* WPCS: xss ok. */ ?></p>
		<?php endif; ?>

		<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
		) );
		?>
	</header>
