<?php
/**
 * posts list for "portfolio" items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autotel2019
 */

get_header();


?>
	<section class="section-container section-title-container">
		<h1>Portfolio</h1>
	</section>
	<section class="section-container section-metadata-container">
		<div class="items-container items-tags-container">
		<?php
			$queriedPosts=get_posts(array(
				"category"=>5,
				"numberposts"=>-1,
			));
			// foreach ($queriedPosts as $post ){
			// 	echo $post->title;
			// 	ptint_r(get_the_tags($post));
			// }
			$terms = get_terms( array(
				'taxonomy' => 'post_tag',
				'hide_empty' => true,
			) );
			foreach ($terms as $key => $term) {
				/*
				example:
				[term_id] => 53 
				[name] => 2010 
				[slug] => year_2010 
				[term_group] => 0 
				[term_taxonomy_id] => 53 
				[taxonomy] => post_tag 
				[description] => 
				[parent] => 0 
				[count] => 3 
				[filter] => raw 
				*/
				?>
				<a class="item-container item-tag-container tag <?php
					//add a class according to before the underscore
					$strpos=strpos($term->slug,"_");
					if($strpos){
						echo substr($term->slug,0,$strpos);
					}?>"<?php
					// add a "supertag" if it has one

					echo ' data-tags="';
					if($strpos){
						echo substr($term->slug,0,$strpos);
					}
					echo '"';
					echo  ' data-slug="'.$term->slug.'"';
					echo  ' data-taxonomy="'.$term->taxonomy.'"';
					echo  ' data-count="'.$term->count.'"';
					echo  ' data-term_id="'.$term->term_id.'"';
					echo  ' data-tags="'.$supertag.'"';
				?> href="<?php
					echo  'tag/'.$term->slug;
				?>"><?php echo $term->name; ?></a>
				<?php
			}
		?>
		</div>
	</section>
	<section class="section-container section-posts-container">
		<div class="items-container items-portfolio-container">
			<?php
			$count=0;
			$posts_max_count=5;
			foreach ($queriedPosts as $count=>$post ){
				/*
				$post->ID			 			int 	The ID of the post
				$post->post_author			 	string 	The post author's user ID (numeric string)
				$post->post_name			 	string 	The post's slug
				$post->post_type			 	string 	See Post Types
				$post->post_title			 	string 	The title of the post
				$post->post_date			 	string 	Format: 0000-00-00 00:00:00
				$post->post_date_gmt		 	string 	Format: 0000-00-00 00:00:00
				$post->post_content			 	string 	The full content of the post
				$post_excerpt				 	string 	User-defined post excerpt
				$post->post_status			 	string 	See get_post_status for values
				$post->comment_status		 	string 	Returns: { open, closed }
				$post->ping_status			 	string 	Returns: { open, closed }
				$post->post_password		 	string 	Returns empty string if no password
				$post->post_parent			 	int 	Parent Post ID (default 0)
				$post->post_modified		 	string 	Format: 0000-00-00 00:00:00
				$post->post_modified_gmt		string 	Format: 0000-00-00 00:00:00
				$post->comment_count		 	string 	Number of comments on post (numeric string)
				$post->menu_order			 	string 	Order value as set through page-attribute when enabled (numeric string. Defaults to 0) 
				
				get_the_tags()
				*/
				$tags=get_the_tags();
				$tagSlugList=Array();
				if($tags) foreach($tags as $key=>$tag){
					array_push($tagSlugList,$tag->slug);
					// $tagSlugList.=$tag->slug." ";
				};

				$thereIsThumbnail=has_post_thumbnail();
				?>
				<a 
					href="<?php echo esc_url( get_permalink() )?>" 
					rel="bookmark"
					class="<?php 
						if($thereIsThumbnail){
							echo "has-image";
						}else{
							echo "without-image";
						}
					?> item-container item-post-container item-portfolio-container"
					data-tags="<?php echo join($tagSlugList," "); ?>"
				>
					<div>
						<?php
						the_title( '<h2 class="title">', '</h2>' );

						?>
						<div class="preview">
						<?php
								if($thereIsThumbnail){
									// echo '<img class="image" src="'.get_the_post_thumbnail_url().'"/>';
									echo '<img class="image" src="'.the_post_thumbnail('thumbnail').'"/>';
								}else{
									?>
									<div class="html">
										<?php //the_excerpt_html(); ?>
									</div>
									<?php
								}
							?>
						</div>
					</div>
				</a>
				<?php
			}
			?>
		</div>
		
	</section>


<?php
get_footer();
