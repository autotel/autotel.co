<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package autotel2019
 */

?>

	</div><!-- #content -->

	<footer class="section-container section-footer-container">
		<div class="site-info">
			<?php
			/* translators: %s: CMS name, i.e. WordPress. */
			// printf( esc_html__( 'Proudly powered by %s', 'autotel2019' ), 'WordPress' );
			?>
			<span class="sep">  </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				// printf( esc_html__( 'Theme: %1$s by %2$s.', 'autotel2019' ), 'autotel2019', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
