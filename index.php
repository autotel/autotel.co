<?php
/**
 * posts list for "portfolio" items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autotel2019
 */

get_header();
?>
	<section class="section-container section-title-container">
		<h1>Posts</h1>
	</section>
	<section class="section-container section-posts-container">
		<div class="items-container items-posts-container items-generic-posts-container">
			<?php
			if ( have_posts() ){

				if ( is_home() && ! is_front_page() ){
					?>
					<header>
						<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
					</header>
					<?php
				}

				/* Start the Loop */
				while ( have_posts() ){
					the_post();
					
					?>
					<a class="item-container item-post-container" href="<?php echo esc_url( get_permalink() )?>" rel="bookmark">
						<?php
						the_title( '<h2 class="title">', '</h2>' );
						?>
						<div class="preview">
							<?php
							if(has_post_thumbnail()){
								echo '<img class="preview-el" src="'.get_the_post_thumbnail_url().'"/>';
							}else{
								?>
								<div class="preview-el">
									<?php the_excerpt();?>
								</div>
								<?php
							}
							?>
						</div>
					</a>
					<?php

				}
				the_posts_navigation();
			}else{
				get_template_part( 'template-parts/content', 'none' );

			}
			?>
		</div>
	</section>


<?php
get_footer();
