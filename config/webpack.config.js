
const path = require('path');
module.exports = {
    watch: true,
    entry: './src/js/index.js',
    output: {
      path: path.resolve(__dirname, '../js'),
      filename: 'index.js'
    },
    module: {
      rules: [
        {
          test: /\.(js)$/,
          exclude: /node_modules/,
          use: ['babel-loader']
        }
      ]
    },
    resolve: {
      extensions: ['*', '.js']
    },
};