<?php
/**
 * autotel2019 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package autotel2019
 */



if ( ! function_exists( 'autotel2019_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function autotel2019_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on autotel2019, use a find and replace
		 * to change 'autotel2019' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'autotel2019', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'autotel2019' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'autotel2019_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		add_theme_support( 'post-thumbnails' );
		// add_image_size( 'thumbnail', 700, 700 ); 
		
	}
endif;
add_action( 'after_setup_theme', 'autotel2019_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function autotel2019_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'autotel2019_content_width', 640 );
}
add_action( 'after_setup_theme', 'autotel2019_content_width', 0 );

	
/**
* permit upload of zip files
*/
function my_custom_mime_types( $mimes ) {
	
	// New allowed mime types.
	$mimes['svg'] = 'image/svg+xml';
	$mimes['zip'] = 'application/zip';
    $mimes['gz'] = 'application/x-gzip';

	return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );

/**
 * Enqueue scripts and styles.
 */
function autotel2019_scripts() {
	wp_enqueue_style( 'autotel2019-style', get_stylesheet_uri() );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/index.js', array(),'1.0.0',true  );
}
add_action( 'wp_enqueue_scripts', 'autotel2019_scripts' );

/**
 * function to get excerpt with html
 */
function get_the_excerpt_html($ipost = false) {
    $post;
	if($ipost){
		$post=$ipost;
	}else{
		global $post;
	}
	//the_content & get_the_content both will cut the text upon the "<!--read more-->" flag, if in index.
	$content=$post->post_content;//arg to prevent link
	$pos=strpos($content,'<!--more-->');
	if($pos){
		$content=apply_filters( "post",substr($content,0,$pos));//apply_filters( "the_content",);
	}else{
		$content=apply_filters( "post",$content );
	}
	return substr($content,0,$pos);
}
function the_excerpt_html($ipost = false) {
	echo get_the_excerpt_html($ipost);
}
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function shortcode_displayPosts($atts,$content){
	//https://developer.wordpress.org/reference/functions/get_posts/
	//none of this works:
	// if($atts['tag']) $atts['tagged']=$atts['tag'];
	// $atts['tax_query']=array(
	// 		array(
	// 			'taxonomy' => 'post_tag',//tagged /tag
	// 			'field' => 'slug',
	// 			'terms' => $atts['tagged']
	// 		)
	// );
	$contents=preg_split('/[\n ,;]+/',$atts["display"]);
	$posts=get_posts( $atts );
	//stupid wordpress cannot intersect criteria
	// if($atts['tagged'] && $atts['category']){
	// 	foreach($posts as $post){
	// 		$postCategories=get_the_post_categories($post->ID,array($fields=>'names'));
	// 		print_r($postCategories);
	// 		// if (!in_array($atts['category'],$postCategories)){
	// 		// }
	// 	}
	// }

	$contentList=Array();
	$return='<div class="items-container items-posts-container shortcode '.$atts['classes'].'">';
	foreach($posts as $k=>$post){
		$return .='<div class="item-container item-post-container">';
		$aTagOpened=false;
		$contentList[$k]=Array();
		foreach($contents as $k2=>$content){
			if($content=="link"){
				$contentList[$k][$k2]=get_post_permalink($post);
				$return .='<a href="'.$contentList[$k][$k2].'"/>';
				$aTagOpened=true;
			}else if($content=="thumbnail"){
				if(has_post_thumbnail($post)){
					// $contentList[$k][$k2]=get_the_post_thumbnail_url($post,"post-thumbnail");
					$contentList[$k][$k2]=get_the_post_thumbnail_url($post,"thumbnail");
					$return .='<img class="thumbnail" src="'.$contentList[$k][$k2].'"/>';
				}else{
					$contentList[$k][$k2]="no thumb";
					// $return .='<img class="thumbnail" src="'.$contentList[$k][$k2].'"/>';
				}
			}else if($content=="excerpt"||$content=="post_excerpt"){
				$contentList[$k][$k2]=get_the_excerpt_html($post);
				$return .='<p class="excerpt">'.$contentList[$k][$k2].'</p>';
			}else  if($content=="year"){
				$contentList[$k][$k2]=get_the_date('Y',$post);
				$return .='<p class="'.$content.'">'.$contentList[$k][$k2].'</p>';
			}else{
				$contentList[$k][$k2]=$post->{$content};
				$return .='<p class="'.$content.'">'.$contentList[$k][$k2].'</p>';
			}
		}
		if($aTagOpened) $return.='</a>';
		
		$return .='</div>'; 
	}
	$return .='</div>'; 

	return ($return);
}
add_shortcode( 'postlist', 'shortcode_displayPosts' );

/*
dummy ajax test
headsup: a confusing thing anout this is that the actual hook name needs to be prepended with wp_ajax
 */

// add_action("wp_ajax_dummy_test_function", "adtf");
// add_action("wp_ajax_nopriv_dummy_test_function","adtf");//for non logged in users
// function adtf() {

// //    if ( !wp_verify_nonce( $_REQUEST['nonce'], "my_user_vote_nonce")) {
// //       exit("No naughty business please");
// //    }   
//    $result=Array("testDummy"=>"it works","request"=>$_REQUEST);
//    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
//       $result = json_encode($result);
//       echo $result;
//    }else {
//       header("Location: ".$_SERVER["HTTP_REFERER"]);
//    }
//    die();

// }
/*
	get posts as json. Used for AJAX
*/

// add_action("wp_ajax_postJsonQuery", "postJsonQuery");
// add_action("wp_ajax_nopriv_postJsonQuery","postJsonQuery");//for non logged in users
// //the f**ing custom wp queries are failing miserably. Retry when in main domain.
// function postJsonQuery(){
// 	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
// 		//parametrize for security.. although this is probably redundant
// 		$query=Array(
// 			'posts_per_page'   => $_REQUEST.posts_per_page,
// 			'offset'           => $_REQUEST.offset,
// 			'cat'        	   => $_REQUEST.cat,
// 			'category_name'    => $_REQUEST.category_name,
// 			'orderby'          => $_REQUEST.orderby,
// 			'order'            => $_REQUEST.order,
// 			// 'include'          => $_REQUEST.include,//causes a syntax error. too lazy to fix
// 			'exclude'          => $_REQUEST.exclude,
// 			'meta_key'         => $_REQUEST.meta_key,
// 			'meta_value'       => $_REQUEST.meta_value,
// 			'post_type'        => $_REQUEST.post_type,
// 			'post_mime_type'   => $_REQUEST.post_mime_type,
// 			'post_parent'      => $_REQUEST.post_parent,
// 			'author'	       => $_REQUEST.author,
// 			'author_name'	   => $_REQUEST.author_name,
// 			'post_status'      => $_REQUEST.post_status,
// 			'suppress_filters' => $_REQUEST.suppress_filters,
// 			'fields'           => $_REQUEST.fields,
// 		);
// 		$queriedPosts=get_posts($query);
// 		$result = json_encode(Array("result"=>$queriedPosts,"request"=>$_REQUEST));
// 		echo $result;
// 	}else {
// 		header("Location: ".$_SERVER["HTTP_REFERER"]);
// 	}
// 	die();
// }

